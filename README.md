# fw-binary

## See project description here [FW image for TTC-FC7](https://cms-tracker-app.web.cern.ch/tkdaq-tasks/task.php?id=70)

## Versions
- **2024.03.21** New features: allow to request exact number of L1A-s to be transmitted **ttc-fc7_2024-03-21.bit**
- **2024.03.20** New features: made the beginning of the abort gap also configurable **ttc-fc7_2024-03-20.bit**
- **2024.03.08** New features: DIO5 CH3 became an input for a trigger inhibit signal (high state disables reading triggers from CH2); new average trigger frequency measurement; prescale factor applied on final L1A to reduce trigger rate sent to the crate. **ttc-fc7_2024-03-08.bit**
- **2023.11.24** New features: configurable orbit gap and trigger phase acceptance window inside the LHC clock, triggers falling outside selection logged as L1 Rejects; configurable trigger latency in units of BX; stable, automatically calibrated phase measurement in 20 bins over an LHC clock period **ttc-fc7_2023-11-24.bit**
- **2023.07.31** New features: configurable orbit length and capability of receiving/distributing stub triggers via backplane **ttc-fc7_2023-07-31.bit**
- **OLD**: 2021.09.10 Version 17 has been tested since Oct 2021: **2021.09.10_veszpv_v17.bit**
